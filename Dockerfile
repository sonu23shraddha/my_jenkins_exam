FROM httpd

COPY ./dist/myapp /usr/local/apache2/htdocs/

CMD apachectl -D FORGROUND

EXPOSE 80
